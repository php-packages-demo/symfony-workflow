# [symfony/workflow](https://packagist.org/packages/symfony/workflow)

Provides tools for managing a workflow or finite state machine

# Official documentation
* Symfony5: The Fast Track [*Step 19: Making Decisions with a Workflow*
  ](https://symfony.com/doc/current/the-fast-track/en/19-workflow.html)

# Unofficial documentation
* [*Introduction to Symfony Workflow*
  ](https://medium.com/@nestorbritomedina/introduction-to-symfony-workflow-10bc4201f52f)
  2024-04 Nestor Brito Medina (Medium)
* [*Reusable symfony/workflow configuration*
  ](https://cwd.at/blog/2023-03-13_symfony_reusable_workflow/)
  2023-03 (cwd.at GmbH)
* [*Implementing a user workflow with Symfony and EasyAdmin3*
  ](https://www.strangebuzz.com/en/blog/implementing-a-user-workflow-with-symfony-and-easyadmin3)
  2021-06 COil
